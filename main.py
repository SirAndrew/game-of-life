import pathlib
import pygame
from pygame.locals import *
from random import randint
from copy import deepcopy
from tkinter import messagebox as message

class GameOfLife:

    def __init__(self, width: int = 640, height: int = 480, cell_size: int = 20, speed: int = 5, max_generations: int = 100) -> None:
        self.width = width
        self.height = height
        self.cell_size = cell_size

        # Устанавливаем размер окна
        self.screen_size = width, height
        # Создание нового окна
        self.screen = pygame.display.set_mode(self.screen_size)

        # Вычисляем количество ячеек по вертикали и горизонтали
        self.cell_width = self.width // self.cell_size
        self.cell_height = self.height // self.cell_size

        # Скорость протекания игры
        self.speed = speed
        # Предыдущее поколение клеток
        self.prev_generation = self.create_grid()
        # Текущее поколение клеток
        self.curr_generation = self.create_grid(True)
        # Максимальное число поколений
        self.max_generations = max_generations
        # Текущее число поколений
        self.generations = 1
        # История
        self.history = [[]]



    def draw_lines(self) -> None:
        for x in range(0, self.width, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('dimgray'),
                             (x, 0), (x, self.height))
        for y in range(0, self.height, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('dimgray'),
                             (0, y), (self.width, y))


    def run(self) -> None:
        pygame.init()
        clock = pygame.time.Clock()
        pygame.display.set_caption('Game of Life')
        running = True
        #grid = self.create_grid(True)
        while running and not self.is_max_generations_exceeded and self.is_changing and not self.check_rep():
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False

            self.step()
            clock.tick(self.speed)
        if (self.is_max_generations_exceeded):
            message.showinfo("Конец игры: Лимит поколений")
        elif (not self.is_changing):
            message.showinfo("Конец игры: Нет движения")
        elif self.check_rep():
            message.showinfo("Конец игры: Повтор состояния")

        pygame.quit()


    def create_grid(self, randomize: bool = False):
        if randomize:
            grid = [[randint(0, 1) for i in range(self.cell_width)] for j in range(self.cell_height)]
        else:
            grid = [[0 for i in range(self.cell_width)] for j in range(self.cell_height)]

        return grid


    def draw_grid(self, grid) -> None:
        for x in range(self.cell_width):
            for y in range(self.cell_height):
                if grid[y][x]:
                    pygame.draw.rect(self.screen, pygame.Color('green'),
                                     (x * self.cell_size + 1, y * self.cell_size + 1, self.cell_size - 1, self.cell_size - 1))


    def get_neighbours(self, cell: tuple, grid):
        neighbours = []
        x, y = cell

        for i in range(y - 1, y + 2):
            for j in  range(x - 1, x + 2):
                tmp_i = i
                tmp_j = j
                if i >= self.cell_height:
                    tmp_i = 0
                if j >= self.cell_width:
                    tmp_j = 0
                neighbours.append(grid[tmp_i][tmp_j])
        return neighbours


    def get_next_generation(self, grid):
        next_grid = self.create_grid(False)
        for x in range(0, self.cell_width):
            for y in range(0, self.cell_height):
                count = 0
                for neighbour in self.get_neighbours((x,y), grid):
                    if neighbour:
                        count += 1

                if grid[y][x]:
                    count -= 1
                    if count == 2 or count == 3:
                        next_grid[y][x] = 1
                    else:
                        next_grid[y][x] = 0
                else:
                    if count == 3:
                        next_grid[y][x] = 1
                    else:
                        next_grid[y][x] = 0
        grid = deepcopy(next_grid)
        return grid


    def step(self) -> None:
        self.screen.fill(pygame.Color('black'))
        self.draw_lines()
        self.draw_grid(self.curr_generation)
        self.append_in_history()
        self.prev_generation = deepcopy(self.curr_generation)
        self.curr_generation = self.get_next_generation(self.curr_generation)
        self.generations += 1
        pygame.display.flip()


    def append_in_history(self):
        self.history.append(self.curr_generation)


    def check_rep(self):
        for matrix in self.history:
            if self.curr_generation == matrix:
                return True
        return False


    @property
    def is_max_generations_exceeded(self) -> bool:
        if self.max_generations != None:
            if self.generations >= self.max_generations:
                return True
        return False


    @property
    def is_changing(self) -> bool:
        if self.curr_generation != self.prev_generation:
            return True
        return False


    @staticmethod
    def from_file(filename: pathlib.Path) -> 'GameOfLife':

        matrix = open(filename).read()
        matrix = [item.split() for item in matrix.split('\n')[:-1]]
        matrix = [[int(item) for item in row] for row in matrix]
        cell_size = matrix[-1][0]
        matrix = matrix[0:-1]
        cell_width = len(matrix[0])
        cell_height = len(matrix)
        width = cell_width * cell_size
        height = cell_height * cell_size
        life = GameOfLife(width, height, cell_size)

        life.curr_generation = deepcopy(matrix)
        return life


    def save(self, filename: pathlib.Path) -> None:
        with open(filename, "w") as f:
            for row in self.curr_generation:
                f.write("%s\n" % ' '.join(str(col) for col in row))
            f.write(f'{self.cell_size}\n')



if __name__ == '__main__':
    game = GameOfLife(1000, 700, 50)
    game.save('save.txt')
    game.run()
    # state = GameOfLife.from_file('save.txt')
    # state.run()

